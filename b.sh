#!/bin/sh

rm -rf .texlive
rm -f paper.{aux,log,out,blg,bbl,spl,pdf}

lualatex paper.tex
bibtex paper.aux
lualatex paper.tex
lualatex paper.tex
